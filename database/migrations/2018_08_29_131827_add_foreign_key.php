<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies_file', function (Blueprint $table) {
            $table->integer('id_company')->unsigned()->change();
            $table->foreign('id_company')->references('id')->on('companies');
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->integer('id_status')->unsigned()->change();
            $table->foreign('id_status')->references('id')->on('companies_status');
        });
        Schema::table('tenders_provider', function (Blueprint $table) {
            $table->integer('id_position')->unsigned()->change();
            $table->foreign('id_position')->references('id')->on('tenders_product');
            $table->integer('id_provider')->unsigned()->change();
            $table->foreign('id_provider')->references('id')->on('companies_provider');
            $table->integer('id_type_provider')->unsigned()>change();
            $table->foreign('id_type_provider')->references('id')->on('companies_type_provider');
        });
        Schema::table('companies_provider', function (Blueprint $table) {
            $table->integer('id_company')->unsigned()->change();
            $table->foreign('id_company')->references('id')->on('companies');
            $table->integer('id_loyalty')->unsigned()->change();
            $table->foreign('id_loyalty')->references('id')->on('companies_loyalty');
            $table->integer('id_honesty')->unsigned()->change();
            $table->foreign('id_honesty')->references('id')->on('companies_honesty');
            $table->integer('id_reliability')->unsigned()->change();
            $table->foreign('id_reliability')->references('id')->on('companies_reliability');
        });
        Schema::table('companies_service', function (Blueprint $table) {
            $table->integer('id_company')->unsigned()->change();
            $table->foreign('id_company')->references('id')->on('companies');
            $table->integer('id_type_service')->unsigned()->change();
            $table->foreign('id_type_service')->references('id')->on('companies_type_service');
        });
        Schema::table('tenders_link', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
        });
        Schema::table('companies_customer', function (Blueprint $table) {
            $table->integer('id_company')->unsigned()->change();
            $table->foreign('id_company')->references('id')->on('companies');
        });
        Schema::table('deals', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
        });
        Schema::table('tenders_product', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
            $table->integer('id_product')->unsigned()->change();
            $table->foreign('id_product')->references('id')->on('products');
        });
        Schema::table('tenders_archive', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
            $table->integer('id_user')->unsigned()->change();
            $table->foreign('id_user')->references('id')->on('users');
        });
        Schema::table('products_dublicate', function (Blueprint $table) {
            $table->integer('id_product')->unsigned()->change();
            $table->foreign('id_product')->references('id')->on('products');
            $table->integer('id_dublicate')->unsigned()->change();
            $table->foreign('id_dublicate')->references('id')->on('products');
        });
        Schema::table('tenders_file', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->integer('id_status')->unsigned()->change();
            $table->foreign('id_status')->references('id')->on('products_status');
            $table->integer('id_responsible')->unsigned()->change();
            $table->foreign('id_responsible')->references('id')->on('users');
        });
        Schema::table('tenders_time_work', function (Blueprint $table) {
            $table->integer('id_tender')->unsigned()->change();
            $table->foreign('id_tender')->references('id')->on('tenders');
            $table->integer('id_user')->unsigned()->change();
            $table->foreign('id_user')->references('id')->on('users');
            $table->integer('id_status')->unsigned()->change();
            $table->foreign('id_status')->references('id')->on('tenders_status');
        });
        Schema::table('tenders', function (Blueprint $table) {
            $table->integer('id_company')->unsigned()->change();
            $table->foreign('id_company')->references('id')->on('companies');
            $table->integer('id_responsible')->unsigned()->change();
            $table->foreign('id_responsible')->references('id')->on('users');
            $table->integer('id_status')->unsigned()->change();
            $table->foreign('id_status')->references('id')->on('tenders_status');
            $table->integer('id_platform')->unsigned()->change();
            $table->foreign('id_platform')->references('id')->on('tenders_platforms');
        });
        Schema::table('tenders_service', function (Blueprint $table) {
            $table->integer('id_position')->unsigned()->change();
            $table->foreign('id_position')->references('id')->on('tenders_product');
            $table->integer('id_service')->unsigned()->change();
            $table->foreign('id_service')->references('id')->on('companies_service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies_file', function (Blueprint $table) {
            $table->dropForeign(['id_company']);
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign(['id_status']);
        });
        Schema::table('tenders_provider', function (Blueprint $table) {
            $table->dropForeign(['id_position']);
            $table->dropForeign(['id_provider']);
            $table->dropForeign(['id_type_provider']);
        });
        Schema::table('companies_provider', function (Blueprint $table) {
            $table->dropForeign(['id_company']);
            $table->dropForeign(['id_loyalty']);
            $table->dropForeign(['id_honesty']);
            $table->dropForeign(['id_reliability']);
        });
        Schema::table('companies_service', function (Blueprint $table) {
            $table->dropForeign(['id_company']);
            $table->dropForeign(['id_type_service']);
        });
        Schema::table('tenders_link', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
        });
        Schema::table('companies_customer', function (Blueprint $table) {
            $table->dropForeign(['id_company']);
        });
        Schema::table('deals', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
        });
        Schema::table('tenders_product', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
            $table->dropForeign(['id_product']);
        });
        Schema::table('tenders_archive', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
            $table->dropForeign(['id_user']);
        });
        Schema::table('products_dublicate', function (Blueprint $table) {
            $table->dropForeign(['id_product']);
            $table->dropForeign(['id_dublicate']);
        });
        Schema::table('tenders_file', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['id_status']);
            $table->dropForeign(['id_responsible']);
        });
        Schema::table('tenders_time_work', function (Blueprint $table) {
            $table->dropForeign(['id_tender']);
            $table->dropForeign(['id_user']);
            $table->dropForeign(['id_status']);
        });
        Schema::table('tenders', function (Blueprint $table) {
            $table->dropForeign(['id_company']);
            $table->dropForeign(['id_responsible']);
            $table->dropForeign(['id_platform']);
            $table->dropForeign(['id_status']);
        });
        Schema::table('tenders_service', function (Blueprint $table) {
            $table->dropForeign(['id_service']);
            $table->dropForeign(['id_position']);
        });
    }
}
