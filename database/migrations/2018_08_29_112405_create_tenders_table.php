<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_status');
            $table->integer('id_company')->nullable();
            $table->integer('id_platform');
            $table->integer('priority')->nullable();
            $table->string('link')->nullable();
            $table->string('comment')->nullable();
            $table->integer('id_responsible');
            $table->timestamp('date_end_application')->nullable();
            $table->timestamp('date_of_summarizing')->nullable();
            $table->integer('price')->nullable();
            $table->string('cause')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders');
    }
}
