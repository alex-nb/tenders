<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders_provider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_position');
            $table->integer('id_provider');
            $table->integer('id_type_provider');
            $table->integer('knowledge')->default(0);
            $table->integer('participation')->default(0);
            $table->integer('reservation')->default(0);
            $table->string('path_commercial')->nullable();
            $table->timestamp('date_download')->nullable();
            $table->integer('price')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders_provider');
    }
}
