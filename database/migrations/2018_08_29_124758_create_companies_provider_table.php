<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_provider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_company');
            $table->string('public_name');
            $table->string('legal_name')->nullable();
            $table->string('site')->nullable();
            $table->string('email')->nullable();
            $table->integer('all_tenders')->default(0);
            $table->integer('all_product')->default(0);
            $table->integer('id_loyalty')->nullable();
            $table->integer('id_honesty')->nullable();
            $table->integer('id_reliability')->nullable();
            $table->integer('raiting')->default(0);
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_provider');
    }
}
