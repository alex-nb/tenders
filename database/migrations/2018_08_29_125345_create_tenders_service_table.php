<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_position');
            $table->integer('id_service');
            $table->integer('price')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('amount')->nullable();
            $table->timestamp('date_delivery')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders_service');
    }
}
