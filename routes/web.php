<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

//Cabinet

Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'Cabinet',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');

        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
            Route::get('/', 'ProfileController@index')->name('home');
            Route::get('/edit', 'ProfileController@edit')->name('edit');
            Route::put('/update', 'ProfileController@update')->name('update');
        });
    }
);

//Admin

Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::resource('users', 'UsersController');
        Route::post('/users/{user}/verify', 'UsersController@verify')->name('users.verify');
    }
);

//Analyst

Route::group(
    [
        'prefix' => 'analyst',
        'as' => 'analyst.',
        'namespace' => 'Analyst',
        'middleware' => ['auth'],
    ],
    function () {
        Route::group(['prefix' => 'tenders', 'as' => 'tenders.'], function () {
            Route::get('/', 'TendersController@index')->name('home');
            //Route::get('/edit', 'ArchiveController@edit')->name('edit');
            //Route::put('/update', 'ArchiveController@update')->name('update');
        });
        Route::group(['prefix' => 'archive', 'as' => 'archive.'], function () {
            Route::get('/', 'ArchiveController@index')->name('home');
        });
    }
);

//Call

Route::group(
    [
        'prefix' => 'call',
        'as' => 'call.',
        'namespace' => 'Call',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');
    }
);

//Director

Route::group(
    [
        'prefix' => 'director',
        'as' => 'director.',
        'namespace' => 'Director',
        'middleware' => ['auth'],
    ],
    function () {
        Route::group(['prefix' => 'check_product', 'as' => 'check_product.'], function () {
            Route::get('/', 'CheckProductController@index')->name('home');
        });
        Route::group(['prefix' => 'all_products', 'as' => 'all_products.'], function () {
            Route::get('/', 'AllProductsController@index')->name('home');
        });
        Route::group(['prefix' => 'companies', 'as' => 'companies.'], function () {
            Route::get('/', 'CompaniesController@index')->name('home');
        });
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('/', 'SettingsController@index')->name('home');
        });
        Route::group(['prefix' => 'tenders', 'as' => 'tenders.'], function () {
            Route::get('/', 'TendersController@index')->name('home');
        });
    }
);

//Manager

Route::group(
    [
        'prefix' => 'manager',
        'as' => 'manager.',
        'namespace' => 'Manager',
        'middleware' => ['auth'],
    ],
    function () {
        Route::group(['prefix' => 'tenders', 'as' => 'tenders.'], function () {
            Route::get('/', 'TendersController@index')->name('home');
        });
        Route::group(['prefix' => 'all_tenders', 'as' => 'all_tenders.'], function () {
            Route::get('/', 'AllTendersController@index')->name('home');
        });
        Route::group(['prefix' => 'control', 'as' => 'control.'], function () {
            Route::get('/', 'ControlController@index')->name('home');
        });
        Route::group(['prefix' => 'document', 'as' => 'document.'], function () {
            Route::get('/', 'DocumentController@index')->name('home');
        });
        Route::group(['prefix' => 'processed', 'as' => 'processed.'], function () {
            Route::get('/', 'ProcessedTendersController@index')->name('home');
        });
        Route::group(['prefix' => 'transport', 'as' => 'transport.'], function () {
            Route::get('/', 'TransportController@index')->name('home');
        });
    }
);

//Operator

Route::group(
    [
        'prefix' => 'operator',
        'as' => 'operator.',
        'namespace' => 'Operator',
        'middleware' => ['auth'],
    ],
    function () {
        Route::group(['prefix' => 'tenders', 'as' => 'tenders.'], function () {
            Route::get('/', 'TendersController@index')->name('home');
        });

        Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
            Route::get('/', 'ProductController@index')->name('home');
        });
    }
);