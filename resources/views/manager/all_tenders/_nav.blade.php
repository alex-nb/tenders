<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('manager.tenders.home') }}">Тендеры к проработке</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('manager.transport.home') }}">Подбор ТК</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('manager.document.home') }}">Подготовка документов</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('manager.control.home') }}">Контроль задач</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('manager.processed.home') }}">Обработанные тендеры</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('manager.all_tenders.home') }}">Все тендеры</a></li>
</ul>