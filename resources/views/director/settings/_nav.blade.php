<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('director.check_product.home') }}">Товары на проверку</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('director.all_products.home') }}">Товары</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('director.tenders.home') }}">Тендеры</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('director.companies.home') }}">Компании</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('director.settings.home') }}">Настройки</a></li>
</ul>