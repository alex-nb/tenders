<?php

namespace App\Http\Controllers\Analyst;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TendersController extends Controller
{
    public function index()
    {
        return view('analyst.tenders.home');
    }
}
