<?php

namespace App\Http\Controllers\Analyst;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArchiveController extends Controller
{
    public function index()
    {
        return view('analyst.archive.home');
    }
}
