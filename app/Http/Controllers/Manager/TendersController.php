<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TendersController extends Controller
{
    public function index()
    {
        return view('manager.tenders.home');
    }
}
