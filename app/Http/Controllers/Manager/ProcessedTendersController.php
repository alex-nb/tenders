<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcessedTendersController extends Controller
{
    public function index()
    {
        return view('manager.processed.home');
    }
}
