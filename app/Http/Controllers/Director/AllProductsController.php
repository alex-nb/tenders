<?php

namespace App\Http\Controllers\Director;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllProductsController extends Controller
{
    public function index()
    {
        return view('director.all_products.home');
    }
}
