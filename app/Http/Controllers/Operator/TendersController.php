<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TendersController extends Controller
{
    public function index()
    {
        return view('operator.tenders.home');
    }
}
